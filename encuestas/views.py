# coding:utf-8
import json
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from encuestas.models import *
from django.views.decorators.csrf import csrf_exempt
# Create your views here.


#REST api
def indexJSON(request):
	#obtener las ultimas 5 preguntas añadidas al sistema, si lo queremos ordenar de modo descendente agregamos '-' a 'pub-date' por fecha de publicacion y solo las primeras [:5]
	#lo convertimos en lista para que no haya problemas al convertirlo en json
	latest_question_list = list(Question.objects.order_by('pub_date')[:5])
	#la funcion map recorre una lista de elementos
	lista = map(lambda x:x.toDict(), latest_question_list)
	response = HttpResponse(json.dumps(lista,
		ensure_ascii = False,
		encoding='utf-8',
		sort_keys = True, 
		indent = 4, 
		separators = (', ',': ')
	), content_type='application/json')
	#crearemos un header la para la comunicacion http
	#creamos el header en django
	response['Access-Control-Allow-Origin'] = '*'
	#indicamos los metodos con los que se puede atacar a la url
	#O sea le decimos al navegador que solo permita peticiones get y options
	#el servidor le dira al navegador que solo podra hacer una peticion tipo get u options 
	response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
	return response

def detailJSON(request, question_id):
	#como primer parametro le pasamos la clase, luego que la clave primaria sea question_id, esto quiere decir, si hago una busqueda en la clase Questionç
	#cuyo id sea question_id, lo almacenara en la variable 'question', si por alguna razon el id no existe, retornara un error 404
	question=get_object_or_404(Question, pk=question_id)
	#crearemos un header la para la comunicacion http
	#creamos el header en django
	response = HttpResponse(question.toJSON(), content_type='application/json')
	response['Access-Control-Allow-Origin'] = '*'
	#indicamos los metodos con los que se puede atacar a la url
	#O sea le decimos al navegador que solo permita peticiones get y options
	#el servidor le dira al navegador que solo podra hacer una peticion tipo get u options 
	response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
	return response
#la vista de voto estara excenta de usar el csrf token

@csrf_exempt
def voteJSON(request, question_id):
	if request.method == 'OPTIONS':
		response = HttpResponse('options ok')
		response['Access-Control-Allow-Origin'] = '*' 
		response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
		response['Access-Control-Allow-Headers'] = 'Content-Type'
		return response

	p=get_object_or_404(Question, pk=question_id)
	try:
		selected_choice = p.choice_set.get(pk=json.loads(request.body)['choice'])
	except (KeyError, Choice.DoesNotExist):
		response = HttpResponse('error')
		response['Access-Control-Allow-Origin'] = '*' 
		response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
		response['Access-Control-Allow-Headers'] = 'Content-Type'
		return response
	else:
		selected_choice.votes += 1
		selected_choice.save()
		response = HttpResponse('ok')
		response['Access-Control-Allow-Origin'] = '*' 
		response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
		response['Access-Control-Allow-Headers'] = 'Content-Type'
		return response




#Cliente Web
#pasamos como parametro request, podriamos llamarlo 'tamarindo', 'pepito', etc
def index(request):
	#obtener las ultimas 5 preguntas añadidas al sistema, si lo queremos ordenar de modo descendente agregamos '-' a 'pub-date' por fecha de publicacion y solo las primeras [:5]
	latest_question_list = Question.objects.order_by('pub_date')[:5]
	#ejemplo de un como podriamos enviar un parametro de prueba ya iterando directamente en views 
	#output = ', '.join([p.question_text for p in latest_question_list])
	#le pasamos como parametro la direccion de la plantilla
	template='index.html'
	#le pasamos un context, que es un diccionario con los datos, informacion que vamos a pasar a la plantilla, yo lo nombro scope por mera costumbre, tambien podria ser un array de objetos como [{obejto1},{objeto2}]
	scope={
		'latest_question_list':latest_question_list
	}

	#con render le decimos que procese los datos que queremos enviar la plantilla, metemos a request, o sea para poder acceder a todos los datos
	return render(request, template, scope)


def detail(request, question_id):
	#como primer parametro le pasamos la clase, luego que la clave primaria sea question_id, esto quiere decir, si hago una busqueda en la clase Questionç
	#cuyo id sea question_id, lo almacenara en la variable 'question', si por alguna razon el id no existe, retornara un error 404
	question=get_object_or_404(Question, pk=question_id)

	template='detail.html'

	scope={
		'question':question
	}

	return render(request, template, scope)


def results(request, question_id):

	question=get_object_or_404(Question, pk=question_id)

	template='result.html'

	scope={
		'question':question
	}

	return render(request, template, scope)


def vote(request, question_id):
	#obtenemos la pregunta con la cual estamos tratando 
	p=get_object_or_404(Question, pk=question_id)
	#almacenamos la opcion seleccionada, obtenemos del set de preguntas, aquel cuyo pk sea la clave choice del diccionario
	#en request van todos los datos que envia el usuario, entre ello lo que se manda por una peticion request.POST
	#en detail.html creamos una peticion method="POST" y obtenemos lo que pusimos en el input name="choice" de modo que el
	#value="{{choice.id}}" que hayamos elegido ira en ['choice']
	try:
		selected_choice=p.choice_set.get(pk=request.POST['choice'])
	#podemos controlar en cierta medida pero no completamente lo que nos manda el usuario, ej: podria no haber seleccionado una opcion
	#si no hubiera un choice seleccionadio entonces 'selected_choice=p.choice_set.get(pk=request.POST['choice'])' fallaria
	#entonces en vez de hacer una comprobacion previa, lo metemos en un try y si falla
	except (KeyError, Choice.DoesNotExist):
		template='detail.html'
		scope={
			'question':p,
			'error_message':'No has seleccionado ninguna'
		}
		return render(request, template, scope)
	#	
	else:
		#manipulacion de la sesion de usuario. Django a cada usuario se le asigna la sesión, la cuál va a tener varias variables de sesión
		#Ahora añadiremos que, cuando se vote en una pregunta, esta será añadida a una lista de identificadores de pregunta y la vamos a
		#a guardar en la sesión para ello, si hay una cadena de texto 'votes' en la request session (en session se almacenan las variables)
		#de sesion del usuario, entonces si ya exite 'votes' en session, entonces tenemos entendido que sera un array, ya que no hay otra cosa
		#en la aplicacion que nos diga que no es un array 
		if 'votes' in request.session:
			#(p.id) es la pregunta actual 
			request.session['votes'].append(p.id)
		else:
			#creamos un array al que añadimos por defecto el id
			request.session['votes'] = [p.id]
		#las sesiones en Django tienen un sistema que detecta si han sido modificadas o no, para guardarlas o no en memoria correctamente
		#a veces esto no se realiza bien, asi que es aconsejable hacer lo sigte, ya que esto fuerza a que se actualize la sesion de usuario
		#para mostrar esto vamos a configurarlo en encuestas.context_proccessors.py
		request.session.modified = True
		#aumentamos en la opcion los votos
		selected_choice.votes+=1
		#y lo guardamos
		selected_choice.save()
		#reverse, en vez de app/encuesta/id, hacemos lo mismo pero desde el mismo codigo de python
		#Redirect, en vez de procesar una respuesta le decimos a nuestro usuario que va a ser redireccionado a otro sitio, en este caso
		#a la pagina de resultados
	return HttpResponseRedirect(reverse('results', args=(p.id,)))
