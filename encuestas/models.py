# coding:utf8
#import from django database, all models
import json
from django.db import models

# Creamos la clase y hereda todas las propiedades de models.model (contiene toda la lógica para conectar con la base de datos)y añadimos las suyas propias
class Question(models.Model):

	# Englobamos en models y pasamos un keywordArgument como parametro, ej: models.CharField(max_length=140)
	question_text = models.CharField(max_length = 200);

	icon = models.CharField(max_length=50);
	
	pub_date=models.DateTimeField('date published');

	def __str__(self):
		return self.question_text.encode('utf-8') #para cadenas de texto con caracteres especiales

	def toDict(self):
		#hacemos una lista de elementos por los que se puede votar en una pregunta
		votesList = []
		#iteramos por cada una de las opciones
		for c in self.choice_set.all():
			#en votesList vamos añadir un diccionario
			votesList.append({
				'name':c.choice_text,
				'votes':c.votes,
				'id':c.pk
			})
		output = {
			'id':self.id,
			#devolvemos el nombre que va a recibir la aplicacion en ionic
			'name':self.question_text,
			'icon':self.icon,
			'pub_date':self.pub_date.strftime("%Y - %m - %d - %H:%M:%S"),#año mes dia minutos segundos
			'options':votesList
		}
		return output

	def toJSON(self):
		oJson = json.dumps(self.toDict(), ensure_ascii = False,
		encoding='utf-8',
		sort_keys = True, 
		indent = 4, 
		separators = (', ',': '))
		return oJson
		

class Choice(models.Model):

	#el campo question en choice es una clave foranea de Question, o sea, es un id que referencia a otro objeto de otra tabla
	question = models.ForeignKey(Question);

	choice_text = models.CharField(max_length=140);
	
	votes = models.IntegerField(default=0);

	def __str__(self):
		return self.choice_text.encode('utf-8') #para cadenas de texto con caracteres especiales