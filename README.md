# APLICACION HECHA CON DJANGO #

Aplicación de encuestas hecha en python-django lista para servir datos a una aplicacíon para plataforma movil hecha en Ionic framework, el link del repo de ionic es [Ionic-Polls](https://bitbucket.org/eliezerhamzfer/ionicpolls)

* En branch master está la versión para montarlo en localhost
* El branch beta lo uso para testing devel

### Requisitos para desarollo ###

* Versión mínima con la que ha sido creado el proyecto: Django>=1.7,<1.8
* Compatibilidad de Python con MySQL: MySQL-python